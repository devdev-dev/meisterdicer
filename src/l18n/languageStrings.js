'use strict';

/**
 * When editing your questions pay attention to your punctuation. Make sure you use question marks or periods.
 * Make sure the first answer is the correct one. Set at least ANSWER_COUNT answers, any extras will be shuffled in.
 */
module.exports = {
  "en": {
    "translation": {
    }
  },
  "de": {
    "translation": {
      "GAME_NAME": "Meisterwürfel",
      "WELCOME" : '<s>Willkommen bei Meistewürfel.</s><s>Lass uns anfangen!</s>',

      "PROBLEM_NO_DIE" : "<s>Ich muss mindestens einen Würfel werfen können.</s>",
      "PROBLEM_UNHANDLED": "<s>Das habe ich nicht verstanden.</s>",

      "SINGLE_RESULT" : "<s>Du hast eine %s gewürfelt.</s>",
      "MULTIPLE_RESULT_NUMBERS": "<s>Du hast eine %s und eine %s gewürfelt.</s>",
      "MULTIPLE_RESULT_SUM": "<s>Die Summe beträgt %s.</s>",

      "NOTHING TO_REPEAT" : "<s>Es gibt nichts zu wiederholen.</s>",
      "NEVER_ROLLED_A_DIE" : "<s>Du hast bisher noch nicht gewürfelt.</s>",

      "PLAY_AGAIN_OR_REPEAT" : "<s>Möchtest du das ich noch einmal würfele oder das Ergebnis wiederhole?</s>",

      "HELP_LIGHT": "<s>Du kannst jederzeit würfeln oder meine Hilfe aktivieren.</s>",
      "HELP" : "<s>Um zu würfeln sage etwas in der Art: „Wirf zwei Würfel mit vier Seiten“.</s>",
      "MORE_HELP" : "<s>Du kannst die Anzahl an Würfel und Seiten selbst festlegen.</s>",

      "ALL_OVER" : "<s>Das Würfelspiel wird jetzt beendet.</s>",
    }
  }
};
