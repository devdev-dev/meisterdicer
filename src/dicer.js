"use strict";

module.exports.rollMultipleDie = function (alexa, count, sides) {
  let numbers = [count];
  for (let i = 0; i < count; i++) {
    numbers[i] = getRandomInt(1, sides);
  }

  const answerSum = alexa.t('MULTIPLE_RESULT_SUM', numbers.reduce((pv, cv) => pv + cv, 0));
  const lastNumber = numbers.pop();
  const answerNumbers = alexa.t('MULTIPLE_RESULT_NUMBERS', numbers.join("; "), lastNumber);

  return {numbers: answerNumbers, sum: answerSum};
};

module.exports.rollSingleDie = function (alexa, dieSides) {
  return alexa.t('SINGLE_RESULT', getRandomInt(1, dieSides));
};

/**
 * Returns a random integer between min and max (inclusive)
 */
function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
