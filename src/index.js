"use strict";

const APP_ID = 'amzn1.ask.skill.e997e835-dba4-4639-b353-56e2177dff97';

const Alexa = require("alexa-sdk");
const Dicer = require('./dicer');

exports.handler = function (event, context, callback) {
  const alexa = Alexa.handler(event, context);
  alexa.appId = APP_ID;

  alexa.resources = require('./l18n/languageStrings');

  alexa.registerHandlers(handlers);
  alexa.execute();
};

const handlers = {
  "LaunchRequest": gameStartResponse,
  "AMAZON.StartOverIntent": gameStartResponse,
  "DiceRollIntent": function () {
    let dieCount = parseInt(this.event.request.intent.slots.count.value);
    let dieSides = parseInt(this.event.request.intent.slots.sides.value);

    rollAndAnswerResponse(this, dieCount, dieSides);
  },
  "DiceRollAgainIntent": function () {
    let dieCount = this.attributes.lastDieCount;
    let dieSides = this.attributes.lastDieSides;

    if (dieCount && dieSides) {
      rollAndAnswerResponse(this, dieCount, dieSides);
    } else {
      this.emit(":ask", this.t("NEVER_ROLLED_A_DIE") + this.t("HELP_LIGHT"), this.t("HELP_LIGHT"));
    }
  },
  "RepeatResultIntent": repeatResponse,
  "AMAZON.CancelIntent": exitResponse,
  "AMAZON.StopIntent": exitResponse,
  "AMAZON.HelpIntent": helpResponse,
  "Unhandled": unhandledResponse,
};

function gameStartResponse() {

  Object.assign(this.attributes, {
    "openSession": true,
  });

  this.emit(":ask", this.t("WELCOME") + this.t("HELP_LIGHT"));
}

function repeatResponse() {
  const result = this.attributes.lastResult;
  if (result) {
    this.emit(':ask', result + this.t("PLAY_AGAIN_OR_REPEAT"), this.t("PLAY_AGAIN_OR_REPEAT"));
  } else {
    this.emit(':ask', this.t("NOTHING TO_REPEAT") + this.t("HELP_LIGHT"), this.t("HELP_LIGHT"));
  }
}

function helpResponse() {
  if (this.attributes.openSession) {
    this.emit(":ask", this.t("HELP") + this.t("MORE_HELP"), this.t("HELP") + this.t("MORE_HELP"));
  } else {
    this.emit(":tell", this.t("HELP") + this.t("MORE_HELP"));
  }
}

function unhandledResponse() {
  this.emit(':ask', this.t("PROBLEM_UNHANDLED") + this.t("HELP_LIGHT"), this.t("HELP_LIGHT"));
}

function exitResponse() {
  this.emit(':tell', this.t("ALL_OVER"));
}

function rollAndAnswerResponse(alexa, dieCount, dieSides) {

  dieCount = dieCount ? dieCount : 1;
  dieSides = dieSides ? dieSides : 6;

  if (dieCount < 1) {
    alexa.emit(':tell', alexa.t("PROBLEM_NO_DIE"));
  } else {

    let result = "";
    if (dieCount === 1) {
      result = Dicer.rollSingleDie(alexa, dieSides);
    } else {
      result = Dicer.rollMultipleDie(alexa, dieCount, dieSides);
      result = result.numbers + result.sum;
    }

    Object.assign(alexa.attributes, {
      "openSession": true,
      "lastDieCount": dieCount,
      "lastDieSides": dieSides,
      "lastResult": result
    });

    alexa.emit(':ask', result + alexa.t("PLAY_AGAIN_OR_REPEAT"), alexa.t("PLAY_AGAIN_OR_REPEAT"));
  }
}
